$(document).ready(function(){
  var isMobile = false;
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    isMobile = true;
  }

  $('.owl-slider-full').owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    items: 3,
    paginationSpeed: 400,
    autoPlay: 5000,
    singleItem: true,
    navigationText: ['<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-left.png" width="23" alt="">', '<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-right.png" width="23" alt="">']
  });

  $('.owl-slider-style2').owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    items: 2,
    itemsDesktop: [1200, 2],
    itemsTablet: [800, 2],
    itemsMobile: [700, 1],
    paginationSpeed: 400,
    navigationText: ['<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-left.png" width="23" alt="">', '<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-right.png" width="23" alt="">']
  });

  $('.owl-slider-style3').owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    items: 3,
    itemsDesktop: [1200, 4],
    itemsTablet: [800, 2],
    itemsMobile: [700, 1],
    paginationSpeed: 400,
    navigationText: ['<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-left.png" width="23" alt="">', '<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-right.png" width="23" alt="">']
  });

  $('.owl-slider-style4').owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    items: 3,
    itemsDesktop: [1200, 3],
    itemsTablet: [991, 3],
    itemsMobile: [767, 1],
    paginationSpeed: 400,
    navigationText: ['<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-left.png" width="23" alt="">', '<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-right.png" width="23" alt="">']
  });

  $('.testimonial-style3').owlCarousel({
    navigation: false,
    items: 3,
    itemsDesktop: [1200, 3],
    itemsTablet: [800, 2],
    itemsMobile: [700, 1]
  });

  $('.gallery-style4').owlCarousel({
    navigation: false,
    items: 4,
    itemsDesktop: [1200, 4],
    itemsTablet: [991, 3],
    itemsMobile: [767, 1]
  });

  $('.owl-slider-auto').owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    items: 3,
    autoPlay: 5000,
    paginationSpeed: 400,
    singleItem: true,
    navigationText: ['<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-left.png" width="23" alt="">', '<img src="https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/gallery-right.png" width="23" alt="">']
  });
});
