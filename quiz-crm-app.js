$(document).ready(function () {
  var question1Answer
  var question2Answer
  var question3Answer
  var question4Answer

  window.logState = function () {
    console.log("Question 1: " + question1Answer)
    console.log("Question 2: " + question2Answer)
    console.log("Question 3: " + question3Answer)
    console.log("Question 4: " + question4Answer)
  }
  // Entrance animations
  $("#headerIntro h1").delay(500).animate({ opacity: "1" }, 1000)
  $("#headerIntro h2").delay(700).animate({ opacity: "1" }, 1000)
  $("#containerStart").delay(1500).fadeIn(750)

  // Click to start quiz
  $("#startQuiz").click(function () {
    // Fade out old text
    $("#containerStart").delay(500).fadeOut(750)
    $("#headerIntro").delay(500).fadeOut(750)
    // Show new text
    $("#navWrap").delay(1250).fadeIn(750)
    // Animate background image transition
    $("#heroImageStart").delay(750).animate(
      {
        opacity: "0"
      },
      500
    )
    $("#heroImage1").delay(500).css({
      display: "block"
    })
    $("#heroImage1").delay(500).animate(
      {
        opacity: "1"
      },
      500
    )
    setTimeout(function () {
      $("#containerQuestion1").show()
      // Bring in first question
      $("#containerQuestion1").animate(
        {
          left: "0px"
        },
        600
      )
      // Update nav menu
      $("#question1Nav").addClass("fill-nav")
      $("#question1Nav").addClass("bg-gold")
    }, 1250)
  })

  // Reusable function for when user selects an option
  function question1Click() {
    // Fade out old text
    $("#containerQuestion1").fadeOut(750)
    // Animate background image transition
    $("#heroImage1").animate(
      {
        opacity: "0"
      },
      500
    )
    $("#heroImage2").css({
      display: "block"
    })
    $("#heroImage2").animate(
      {
        opacity: "1"
      },
      500
    )
    // Update nav menu
    $("#question2Nav").addClass("fill-nav")
    $("#question2Nav").addClass("bg-gold")
    $("#questionTitle").fadeOut(function () {
      $(this).text("QUESTION 2:").fadeIn()
    })
    setTimeout(function () {
      // Show new text
      $("#containerQuestion2").show()
      // Bring in first question
      $("#containerQuestion2").animate(
        {
          left: "0px"
        },
        600
      )
    }, 1250)
  }

  // When user clicks a question 1 answer, assign them a value and progress the quiz
  $("#question1Answer1").click(function () {
    question1Answer = "Small"
    question1Click()
  })

  $("#question1Answer2").click(function () {
    question1Answer = "Medium"
    $("#question2Answer3").show()
    question1Click()
  })

  $("#question1Answer3").click(function () {
    question1Answer = "Large"
    $("#question2Answer3").show()
    question1Click()
  })

  $("#goToQuestion1").click(function () {
    // Go back to Q1
    $("#containerQuestion2").animate(
      {
        left: "100%"
      },
      750
    )
    setTimeout(function () {
      $("#containerQuestion2").hide()
    }, 850)
    $("#heroImage2").animate(
      {
        opacity: "0"
      },
      500
    )
    $("#heroImage1").css({
      display: "block"
    })
    $("#heroImage1").animate(
      {
        opacity: "1"
      },
      500
    )
    $("#question2Nav").removeClass("fill-nav")
    $("#question2Nav").removeClass("bg-gold")
    $("#questionTitle").fadeOut(function () {
      $(this).text("QUESTION 1:").fadeIn()
    })
    setTimeout(function () {
      $("#containerQuestion1").fadeIn(750)
    }, 1000)
    question1Answer = ""
  })

  function narrowEarlyButtons() {
    // Only show buttons relevant to the user's choices
    if (question1Answer === "Small" && question2Answer === "Less") {
      // Origin Guatemala
      $("#question4Answer4").show()
      // Origin Brazil
      $("#question4Answer5").show()
      $("#question4Answer9").show()
    } else if (question1Answer === "Small" && question2Answer === "More") {
      // Ristretto Intenso
      $("#question4Answer1").show()
      // Ristretto
      $("#question4Answer2").show()
    }
  }

  // Reusable function for when user selects an option
  function question2Click() {
    if (question1Answer === "Small" && question2Answer === "hgd") {
      // Run code to show question 4 as question 3
      narrowEarlyButtons()
      // Fade out old text
      $("#containerQuestion2").fadeOut(750)
      // Animate background image transition
      $("#heroImage2").animate(
        {
          opacity: "0"
        },
        500
      )
      $("#heroImage4").css({
        display: "block"
      })
      $("#heroImage4").animate(
        {
          opacity: "1"
        },
        500
      )
      if (question2Answer === "More") {
        $("#galleryMild").hide()
        $("#galleryIntense").show()
      }
      // Update nav menu
      $("#question3Nav").addClass("fill-nav")
      $("#question3Nav").addClass("bg-gold")
      $("#questionTitle").fadeOut(function () {
        $(this).text("QUESTION 3:").fadeIn()
      })
      setTimeout(function () {
        // Show new text
        $("#containerQuestion4").show()
        // Bring in first question
        $("#containerQuestion4").animate(
          {
            left: "0px"
          },
          600
        )
      }, 1250)
    } else {
      // Fade out old text
      $("#containerQuestion2").fadeOut(750)
      // Animate background image transition
      $("#heroImage2").animate(
        {
          opacity: "0"
        },
        500
      )
      $("#heroImage3").css({
        display: "block"
      })
      $("#heroImage3").animate(
        {
          opacity: "1"
        },
        500
      )
      if (question2Answer === "More") {
        $("#galleryMild").hide()
        $("#galleryIntense").show()
        $("#question3Answer3").show()
      }
      // Update nav menu
      $("#question3Nav").addClass("fill-nav")
      $("#question3Nav").addClass("bg-gold")
      $("#questionTitle").fadeOut(function () {
        $(this).text("QUESTION 3:").fadeIn()
      })
      setTimeout(function () {
        // Show new text
        $("#containerQuestion3").show()
        // Bring in first question
        $("#containerQuestion3").animate(
          {
            left: "0px"
          },
          600
        )
      }, 1250)
    }
  }

  // When user clicks a question 2 answer, assign them a value and progress the quiz
  $("#question2Answer1").click(function () {
    question2Answer = "Less"
    question2Click()
  })

  $("#question2Answer2").click(function () {
    question2Answer = "More"
    question2Click()
  })

  $("#question2Answer3").click(function () {
    question2Answer = "None"
    question2Click()
  })

  $("#goToQuestion2").click(function () {
    // Go back to Q2
    $("#containerQuestion3").animate(
      {
        left: "100%"
      },
      750
    )
    setTimeout(function () {
      $("#containerQuestion3").hide()
    }, 850)
    $("#heroImage3").animate(
      {
        opacity: "0"
      },
      500
    )
    $("#heroImage2").css({
      display: "block"
    })
    $("#heroImage2").animate(
      {
        opacity: "1"
      },
      500
    )
    $("#question3Nav").removeClass("fill-nav")
    $("#question3Nav").removeClass("bg-gold")
    $("#questionTitle").fadeOut(function () {
      $(this).text("QUESTION 2:").fadeIn()
    })
    setTimeout(function () {
      $("#containerQuestion2").fadeIn(750)
    }, 1000)
    question2Answer = ""
  })

  // Reusable function for when user selects an option
  function question3Click() {
    // Fade out old text
    $("#containerQuestion3").fadeOut(750)
    // Animate background image transition
    $("#heroImage3").animate(
      {
        opacity: "0"
      },
      500
    )
    $("#heroImage4").css({
      display: "block"
    })
    $("#heroImage4").animate(
      {
        opacity: "1"
      },
      500
    )
    // Update nav menu
    $("#question4Nav").addClass("fill-nav")
    $("#question4Nav").addClass("bg-gold")
    $("#questionTitle").fadeOut(function () {
      $(this).text("QUESTION 4:").fadeIn()
    })
    setTimeout(function () {
      // Show new text
      $("#containerQuestion4").show()
      // Bring in first question
      $("#containerQuestion4").animate(
        {
          left: "0px"
        },
        600
      )
    }, 1250)
  }

  function narrowQuestion4Buttons() {
    // Only show buttons relevant to the user's choices
    if (question1Answer === "Small" && question2Answer === "Less" && question3Answer === "A") {
      // Origin Brazil
      $("#question4Answer5").show()
      // Bianco Delicato
      $("#question4Answer9").show()
      // Go to quesiton 4
      question3Click()
    } else if (question1Answer === "Medium" && question2Answer === "Less" && question3Answer === "A") {
      // Espresso Origin Brazil
      $("#question4Answer5").show()
      // Peru Organic
      $("#question4Answer6").show()
      // Bianco Delicato
      $("#question4Answer9").show()
      // Go to quesiton 4
      question3Click()
    } else if (question1Answer === "Medium" && question2Answer === "Less" && question3Answer === "AR") {
      // Lungo Origin Guatemala
      $("#question4Answer4").show()
      // Espresso Legerro
      $("#question4Answer3").show()
      // Go to quesiton 4
      question3Click()
    } else if (question1Answer === "Medium" && question2Answer === "More" && question3Answer === "A") {
      // Espresso Caramel
      $("#question4Answer9").show()
      // Espresso Vanilla
      $("#question4Answer8").show()
      // Forte
      $("#question4Answer3").show()
      // Go to quesiton 4
      question3Click()
    } else if (question1Answer === "Large" && question2Answer === "Less" && question3Answer === "A") {
      // Lungo Decaffeinato
      $("#question4Answer4").show()
      // Lungo Forte
      $("#question4Answer6").show()
      // Espresso Origin Brazil
      $("#question4Answer5").show()
      // Finezzo
      $("#question4Answer7").show()
      // Go to quesiton 4
      question3Click()
    } else if (question1Answer === "Small" && question2Answer === "More" && question3Answer === "AR") {
      // Ristretto Intenso
      $("#question4Answer1").show()
      // Origin India
      $("#question4Answer4").show()
      // BIanco Intenso
      $("#question4Answer10").show()
      // Go to quesiton 4
      question3Click()
    } else if (question1Answer === "Large" && question2Answer === "More" && question3Answer === "AR") {
      // Ristretto Intenso
      $("#question4Answer11").show()
      // Origin India
      $("#question4Answer10").show()
      // Go to quesiton 4
      question3Click()
    } else if (question1Answer === "Medium" && question2Answer === "More" && question3Answer === "AR") {
      // Ristretto Intenso
      $("#question4Answer4").show()
      // Origin India
      $("#question4Answer11").show()
      // Go to quesiton 4
      question3Click()
    }
  }

  // Load EARLY results screen
  function loadEarlyResultScreen() {
    // Update nav menu
    $("#question4Nav").addClass("fill-nav")
    $("#question4Nav").addClass("bg-gold")
    // Fade out old text
    $("#containerQuestion3").fadeOut(750)
    $("#headerIntro").fadeOut(750)
    $("#navWrap").fadeOut(750)
    // Animate background image transition
    $("#heroImage3").animate(
      {
        opacity: "0"
      },
      500
    )
    $("#heroImageFinal").css({
      display: "block"
    })
    $("#heroImageFinal").animate(
      {
        opacity: "1"
      },
      500
    )
    setTimeout(function () {
      // Show new text
      $("#headerSuggestion").fadeIn()
      $("#suggestionImage").fadeIn()
      $("#containerSuggestion").show()
      $("#productDetail").show()
      // Bring in first question
      $("#containerSuggestion").animate(
        {
          left: "0px"
        },
        600
      )
      $("#machineWrapper").slideDown(500)
      $("html, body").animate(
        {
          scrollTop: $("#banner").offset().top
        },
        650
      )
    }, 1250)
  }

  function earlyDelivery() {
    if (question1Answer === "Large" && question2Answer === "Less" && question3Answer === "AR") {
      //Show Origin Guatemala
      $("#flavourName").text("ORIGIN GUATEMALA")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-guat-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-guatemala-coffee-capsule")
      $("#intensityValue").html("6")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "75px"
          },
          850
        )
      }, 2100)
      $("#galleryImg3").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-espresso-caramel-gold.png")
      $("#galleryImg3Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-caramel")
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p><strong>Nespresso<sup>&reg;</sup></strong> Guatemala's intense dry and malty cereal notes are tempered by its elegant bitterness and a silky-smooth texture.</p><p>You’ll catch that classic cereal note of Robusta, but it’s laced with a fine, dry vegetal note that adds to the blend’s complexity. Guatemala is a bold coffee, but its strength is disarming.</p>")
      loadEarlyResultScreen()
    } else if (question1Answer === "Medium" && question2Answer === "None" && question3Answer === "A") {
      // Espresso Decaffeinato
      $("#flavourName").text("ESPRESSO DECAFFEINATO")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-decaffeinato.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-espresso-decaffeinato")
      $("#intensityValue").html("7")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "88px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img6").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img6Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#productDetail").html("<p>Espresso Decaffeinato is rich in aroma, both in the coffee and in its thick crema. You can catch a subtle cocoa and powerful roasted cereal note in this full-bodied decaffeinated espresso.</p><p>The grilled aromas give this <strong>Nespresso<sup>&reg;</sup></strong> decaf an intensity that’ll compel you at any time of the day or night.</p>")
      loadEarlyResultScreen()
    } else if (question1Answer === "Medium" && question2Answer === "None" && question3Answer === "AR") {
      // Espresso Decaffeinato
      $("#flavourName").text("ESPRESSO DECAFFEINATO")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-decaffeinato.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-espresso-decaffeinato")
      $("#intensityValue").html("7")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "88px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img6").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img6Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#productDetail").html("<p>Espresso Decaffeinato is rich in aroma, both in the coffee and in its thick crema. You can catch a subtle cocoa and powerful roasted cereal note in this full-bodied decaffeinated espresso.</p><p>The grilled aromas give this <strong>Nespresso<sup>&reg;</sup></strong> decaf an intensity that’ll compel you at any time of the day or night.</p>")
      loadEarlyResultScreen()
    } else if (question1Answer === "Large" && question2Answer === "None" && question3Answer === "A") {
      // Lungo Decaffeinato
      $("#flavourName").text("LUNGO DECAFFEINATO")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-lung-decaf.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-decaffeinato-box")
      $("#intensityValue").html("4")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "50px"
          },
          850
        )
      }, 2100)
      $("#galleryImg6").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg6Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p><strong>Nespresso<sup>&reg;</sup></strong> Lungo Decaffeinato is silky and soft. In this decaffeinated long black coffee, you’ll get the full palate – from the toasted cereal notes to the sweet florals.</p>")
      loadEarlyResultScreen()
      /*
    } else if (question1Answer === "Medium" && question2Answer === "More" && question3Answer === "AR") {
      // Forte
      $("#flavourName").text("FORTE")
      $("#mugImg").attr("src", "./assets/mug-forte.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-forte-box")
      $("#intensityValue").html("7")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "88px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#productDetail").html("<p>A short, medium roast brings out the best of each coffee in this Arabica blend. The coffee roasting develops the Brazil’s malty note and the Costa Rica’s red fruit bouquet – all in a strikingly intense equilibrium.</p>")
      loadEarlyResultScreen()
    */
    } else if (["Medium", "Large"].indexOf(question1Answer) !== false && question2Answer === "More" && question3Answer === "IN") {
      // Intenso
      $("#flavourName").text("INTENSO")
      $("#mugImg").attr("src", "./assets/mug-intenso.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-intenso-box")
      $("#intensityValue").html("8")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "120px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-intenso-box")
      $("#productDetail").html("<p>Intenso has an intense roasted aroma with bold aromatics, it is the most intense coffee that can be extracted in Lungo cup size.</p>")
      loadEarlyResultScreen()
    } else if (question1Answer === "Small" && question2Answer === "More" && question3Answer === "A") {
      // Ristretto
      $("#flavourName").text("RISTRETTO")
      $("#mugImg").attr("src", "./assets/mug-ristretto.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-ristretto-box")
      $("#intensityValue").html("9")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "113px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img5").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img5Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#productDetail").html("<p>This Ristretto coffee has an intense roasty character. Deep cocoa notes and a subtle woody aroma linger long in the rich crema of this full-bodied coffee.</p>")
      loadEarlyResultScreen()
    }
  }

  // When user clicks a question 3 answer, assign them a value and progress the quiz
  $("#question3Answer1").click(function () {
    question3Answer = "A"
    narrowQuestion4Buttons()
    earlyDelivery()
  })

  $("#question3Answer2").click(function () {
    question3Answer = "AR"
    narrowQuestion4Buttons()
    earlyDelivery()
  })
  $("#question3Answer3").click(function () {
    question3Answer = "IN"
    narrowQuestion4Buttons()
    earlyDelivery()
  })

  $("#goToQuestion3").click(function () {
    if (question1Answer === "Small" && false) {
      // Go back to Q2
      $("#containerQuestion4").animate(
        {
          left: "100%"
        },
        750
      )
      setTimeout(function () {
        $("#containerQuestion4").hide()
      }, 850)
      $("#heroImage4").animate(
        {
          opacity: "0"
        },
        500
      )
      $("#heroImage2").css({
        display: "block"
      })
      $("#heroImage2").animate(
        {
          opacity: "1"
        },
        500
      )
      $("#question3Nav").removeClass("fill-nav")
      $("#question3Nav").removeClass("bg-gold")
      $("#questionTitle").fadeOut(function () {
        $(this).text("QUESTION 2:").fadeIn()
      })
      setTimeout(function () {
        $("#containerQuestion2").fadeIn(750)
        $("#question4Answer1").hide()
        $("#question4Answer2").hide()
        $("#question4Answer4").hide()
        $("#question4Answer5").hide()
      }, 1000)
      question2Answer = ""
    } else {
      // Go back to Q3
      $("#containerQuestion4").animate(
        {
          left: "100%"
        },
        750
      )
      setTimeout(function () {
        $("#containerQuestion4").hide()
      }, 850)
      $("#heroImage4").animate(
        {
          opacity: "0"
        },
        500
      )
      $("#heroImage3").css({
        display: "block"
      })
      $("#heroImage3").animate(
        {
          opacity: "1"
        },
        500
      )
      $("#question4Nav").removeClass("fill-nav")
      $("#question4Nav").removeClass("bg-gold")
      $("#questionTitle").fadeOut(function () {
        $(this).text("QUESTION 3:").fadeIn()
      })
      setTimeout(function () {
        $("#containerQuestion3").fadeIn(750)
      }, 1000)
      question3Answer = ""
    }
  })

  // Showing/hiding the different options for small/med/large machines
  $("#machineSmall").click(function () {
    if ($("#machineRevealMedium").is(":hidden") && $("#machineRevealLarge").is(":hidden")) {
      $(this).toggleClass("bg-gold")
      $("#machineRevealSmall").slideToggle(500)
      $("html, body")
        .delay(500)
        .animate(
          {
            scrollTop: $("#machineRevealSmall").offset().top
          },
          650
        )
    }
  })

  $("#machineMedium").click(function () {
    if ($("#machineRevealSmall").is(":hidden") && $("#machineRevealLarge").is(":hidden")) {
      $(this).toggleClass("bg-gold")
      $("#machineRevealMedium").slideToggle(500)
      $("html, body")
        .delay(500)
        .animate(
          {
            scrollTop: $("#machineRevealMedium").offset().top
          },
          650
        )
    }
  })

  $("#machineLarge").click(function () {
    if ($("#machineRevealSmall").is(":hidden") && $("#machineRevealMedium").is(":hidden")) {
      $(this).toggleClass("bg-gold")
      $("#machineRevealLarge").slideToggle(500)
      $("html, body")
        .delay(600)
        .animate(
          {
            scrollTop: $("#machineRevealLarge").offset().top
          },
          650
        )
    }
  })

  // Load results screen
  function goToResults() {
    // Update nav menu
    $("#question4Nav").addClass("fill-nav")
    $("#question4Nav").addClass("bg-gold")
    // Fade out old text
    $("#containerQuestion4").fadeOut(750)
    $("#headerIntro").fadeOut(750)
    $("#navWrap").fadeOut(750)
    // Animate background image transition
    $("#heroImage4").animate(
      {
        opacity: "0"
      },
      500
    )
    $("#heroImageFinal").css({
      display: "block"
    })
    $("#heroImageFinal").animate(
      {
        opacity: "1"
      },
      500
    )
    setTimeout(function () {
      // Show new text
      $("#headerSuggestion").fadeIn()
      $("#suggestionImage").fadeIn()
      $("#containerSuggestion").show()
      $("#productDetail").show()
      // Bring in first question
      $("#containerSuggestion").animate(
        {
          left: "0px"
        },
        600
      )
      $("#machineWrapper").slideDown(500)
      $("html, body").animate(
        {
          scrollTop: $("#banner").offset().top
        },
        650
      )
    }, 1250)
  }

  $("#question4Answer1").click(function () {
    if (question1Answer === "Small" && question2Answer === "More") {
      // Ristretto Intenso
      $("#flavourName").text("RISTRETTO INTENSO")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/ristretto-intenso-origin-india-2-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro")
      $(".intensity-wrap").hide()
      $("#gallery2Img7").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img7Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-ristretto-intenso-box")
      $("#productDetail").html("<p>This Ristretto coffee is a roasty, bitter, full-bodied coffee. Its spicy pepper notes and deep woody aroma are laced through its thick, creamy texture.</p><p>This <strong>Nespresso<sup>&reg;</sup></strong> pro capsule balances out the finer aromatics of an Arabica with the potency of Robusta. Ristretto Intenso really knows how to go courting intensity.</p>")
      goToResults()
    } else if (question1Answer === "Large" && question2Answer === "More") {
      // Origin India
      $("#flavourName").text("ORIGIN INDIA")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-india-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#intensityValue").html("10")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "125px"
          },
          850
        )
      }, 2100)
      $("#productDetail").html("<p>India's aromas echo the coffee’s origin. The woody notes and that noble punch of clove, nutmeg and pepper all reflect what grows between the coffee trees in southern India.</p>")
      goToResults()
    }
  })

  $("#question4Answer3").click(function () {
    if (question1Answer === "Medium" && question2Answer === "Less" && question3Answer === "AR") {
      // Leggero
      $("#flavourName").text("LEGGERO")
      $("#mugImg").attr("src", "./assets/mug-leggero.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-leggero-box")
      $("#intensityValue").html("6")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "75px"
          },
          850
        )
      }, 2100)
      $("#galleryImg7").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg7Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p>This <strong>Nespresso<sup>&reg;</sup></strong> Professional capsule is an intense coffee with strong roasted, toasted notes and jammy red fruit aromas.</p>")
      goToResults()
    } else if (question1Answer === "Medium" && question2Answer === "More" && question3Answer === "A") {
      // Forte
      $("#flavourName").text("FORTE")
      $("#mugImg").attr("src", "./assets/mug-forte.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-forte-box")
      $("#intensityValue").html("7")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "88px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#productDetail").html("<p>A short, medium roast brings out the best of each coffee in this Arabica blend. The coffee roasting develops the Brazil’s malty note and the Costa Rica’s red fruit bouquet – all in a strikingly intense equilibrium.</p>")
      goToResults()
    }
  })

  $("#question4Answer4").click(function () {
    if (question1Answer === "Medium" && question2Answer === "Less" && question3Answer === "AR") {
      // Origin Guatemala
      $("#flavourName").text("ORIGIN GUATEMALA")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-guat-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-guatemala-coffee-capsule")
      $("#intensityValue").html("6")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "75px"
          },
          850
        )
      }, 2100)
      $("#galleryImg3").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-espresso-caramel-gold.png")
      $("#galleryImg3Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-caramel")
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p><strong>Nespresso<sup>&reg;</sup></strong> Guatemala's intense dry and malty cereal notes are tempered by its elegant bitterness and a silky-smooth texture.</p><p>You’ll catch that classic cereal note of Robusta, but it’s laced with a fine, dry vegetal note that adds to the blend’s complexity. Guatemala is a bold coffee, but its strength is disarming.</p>")
      goToResults()
    } else if (question1Answer === "Small" && question2Answer === "Less") {
      // Origin Guatemala
      $("#flavourName").text("ORIGIN GUATEMALA")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-guat-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-guatemala-coffee-capsule")
      $("#intensityValue").html("6")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "75px"
          },
          850
        )
      }, 2100)
      $("#galleryImg3").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-espresso-caramel-gold.png")
      $("#galleryImg3Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-caramel")
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p><strong>Nespresso<sup>&reg;</sup></strong> Guatemala's intense dry and malty cereal notes are tempered by its elegant bitterness and a silky-smooth texture.</p><p>You’ll catch that classic cereal note of Robusta, but it’s laced with a fine, dry vegetal note that adds to the blend’s complexity. Guatemala is a bold coffee, but its strength is disarming.</p>")
      goToResults()
    } else if (question1Answer === "Large" && question2Answer === "Less" && question3Answer === "A") {
      // Lungo Decaffeinato
      $("#flavourName").text("LUNGO DECAFFEINATO")
      $("#mugImg").attr("src", "./assets/mug-lungo-decaffeinato.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-decaffeinato-box")
      $("#intensityValue").html("4")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "50px"
          },
          850
        )
      }, 2100)
      $("#galleryImg6").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg6Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p><strong>Nespresso<sup>&reg;</sup></strong> Lungo Decaffeinato is silky and soft. In this decaffeinated long black coffee, you’ll get the full palate – from the toasted cereal notes to the sweet florals.</p>")
      goToResults()
    } else if ((question1Answer === "Small" || question1Answer === "Medium") && question2Answer === "More" && question3Answer === "AR") {
      //MARK
      // BIANCO INTENSO
      $("#flavourName").text("BIANCO INTENSO")
      $("#mugImg").attr("src", "./assets/mug-bianco-intenso.png") // @OLIVER - Done
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/bianco-intenso-coffee-box")
      $("#intensityValue").html("7")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "88px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img2").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png") // MARK: ?
      $("#gallery2Img2Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule") // MARK: ?
      $("#productDetail").html("<p>BIANCO INTENSO is designed with a meticulous technique of split roasting that delivers the intensity yet fine balance of coffee and milk. Our blend mainly comprised of Colombian Arabicas is roasted in 2 ways; one part is roasted dark, the other is roasted light, so that when milk is added, sweet and nutty cereal notes are revealed to create a harmonious cup of coffee. </p>")
      goToResults()
    }
  })

  $("#question4Answer5").click(function () {
    if (question1Answer === "Small" && question2Answer === "Less") {
      // Espresso Origin Brazil
      $("#flavourName").text("ORIGIN BRAZIL")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-brazil-2-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-brazil-coffee-capsule")
      $("#intensityValue").html("4")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "50px"
          },
          850
        )
      }, 2100)
      $("#galleryImg1").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-espresso-caramel-gold.png")
      $("#galleryImg1Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-caramel")
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p>Origin Brazil is a smooth talker. It’s elegant and delicate, and neither bitter nor acidic.</p><p>Its classic Brazilian sweet cereal notes and hint of toasted grain makes it a winner if you're looking for an exceptionally smooth and mild coffee.</p>")
      goToResults()
    } else if (question1Answer === "Medium" && question2Answer === "Less" && question3Answer === "A") {
      // Espresso Origin Brazil
      $("#flavourName").text("ORIGIN BRAZIL")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-brazil-2-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-brazil-coffee-capsule")
      $("#intensityValue").html("4")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "50px"
          },
          850
        )
      }, 2100)
      $("#galleryImg1").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-espresso-caramel-gold.png")
      $("#galleryImg1Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-caramel")
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p>Origin Brazil is a smooth talker. It’s elegant and delicate, and neither bitter nor acidic.</p><p>Its classic Brazilian sweet cereal notes and hint of toasted grain makes it a winner if you're looking for an exceptionally smooth and mild coffee.</p>")
      goToResults()
    } else if (question1Answer === "Large" && question2Answer === "Less" && question3Answer === "A") {
      // Espresso Origin Brazil
      $("#flavourName").text("ORIGIN BRAZIL")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-brazil-2-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-brazil-coffee-capsule")
      $("#intensityValue").html("4")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "50px"
          },
          850
        )
      }, 2100)
      $("#galleryImg1").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-espresso-caramel-gold.png")
      $("#galleryImg1Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-caramel")
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p>Origin Brazil is a smooth talker. It’s elegant and delicate, and neither bitter nor acidic.</p><p>Its classic Brazilian sweet cereal notes and hint of toasted grain makes it a winner if you're looking for an exceptionally smooth and mild coffee.</p>")
      goToResults()
    }
  })

  $("#question4Answer6").click(function () {
    if ((question1Answer === "Medium" || question1Answer === "Large") && question2Answer === "Less" && question3Answer === "A") {
      // Peru Organic
      $("#flavourName").text("ORIGIN PERU ORGANIC")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-peru-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-peru-organic-coffee-capsule")
      $("#intensityValue").html("6")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "75px"
          },
          850
        )
      }, 2100)
      $("#galleryImg2").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-espresso-caramel-gold.png")
      $("#galleryImg2Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-caramel")
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p>Peru Organic is a fruity coffee with distinct green vegetable notes. The coffee’s fine acidity is beautifully balanced by its contrasting aromas of toasted sweet cereals.</p>")
      goToResults()
    }
  })

  $("#question4Answer7").click(function () {
    if (question1Answer === "Large" && question2Answer === "Less" && question3Answer === "A") {
      // Finezzo
      $("#flavourName").text("FINEZZO")
      $("#mugImg").attr("src", "./assets/mug-finezzo.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-finezzo-box")
      $("#intensityValue").html("5")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "75px"
          },
          850
        )
      }, 2100)
      $("#productDetail").html("<p>This <strong>Nespresso<sup>&reg;</sup></strong> Professional capsule’s washed Arabica coffees give this long coffee its delicate notes of jasmine, orange blossom and bergamot.</p><p>Its gentle acidity sparkles in Finezzo like dew drops on the flower petals of a hidden garden.</p>")
      goToResults()
    }
  })

  $("#question4Answer8").click(function () {
    if (question1Answer === "Medium" && question2Answer === "More" && question3Answer === "A") {
      // Espresso Vanilla
      $("#flavourName").text("ESPRESSO VANILLA")
      $("#mugImg").attr("src", "./assets/mug-vanilla.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-vanilla")
      $("#intensityValue").html("7")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "88px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img1").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img1Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#productDetail").html("<p>Espresso Vanilla is a flavoured coffee with a silky feel and a slightly caramelized aroma of vanilla.</p>")
      goToResults()
    }
  })

  $("#question4Answer9").click(function () {
    if (question1Answer === "Medium" && question2Answer === "More" && question3Answer === "A") {
      // Espresso Caramel
      $("#flavourName").text("ESPRESSO CARAMEL")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-caramel.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/espresso-caramel")
      $("#intensityValue").html("7")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "88px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img2").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png")
      $("#gallery2Img2Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#productDetail").html("<p>This <strong>Nespresso<sup>&reg;</sup></strong> Professional capsule starts as an intense coffee with strong toasted notes and jammy red fruit aromas.</p><p>We layer in the caramel flavour and its sweetness mellows the coffees’ roasted notes and gives this caramel espresso a warm browned sugar aroma.</p>")
      goToResults()
    }
    if ((question1Answer === "Small" || question1Answer === "Medium") && question2Answer === "Less" && question3Answer === "A") {
      //MARK
      // BIANCO DELICATO
      $("#flavourName").text("BIANCO DELICATO")
      $("#mugImg").attr("src", "./assets/mug-bianco.png") // @OLIVER - Done
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/bianco-delicato-coffee-box")
      $("#intensityValue").html("6")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "75px"
          },
          850
        )
      }, 2100)
      $("#gallery2Img2").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-origin-india-gold.png") // MARK: ?
      $("#gallery2Img2Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule") // MARK: ?
      $("#productDetail").html("<p>BIANCO DELICATO is a coffee carefully designed for a great harmony of coffee with milk. Our blend predominantly composed of Kenyan Arabicas is very light roasted to bring out the natural sweetness of the coffee and reveals delicious caramel and biscuit notes when milk is added.</p>")
      goToResults()
    }
  })

  $("#question4Answer10").click(function () {
    if ((question1Answer === "Small" || question1Answer === "Large") && question2Answer === "More" && question3Answer === "AR") {
      // Origin India
      $("#flavourName").text("ORIGIN INDIA")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-india-c.png")
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/origin-india-coffee-capsule")
      $("#intensityValue").html("10")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "125px"
          },
          850
        )
      }, 2100)
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p>India's aromas echo the coffee’s origin. The woody notes and that noble punch of clove, nutmeg and pepper all reflect what grows between the coffee trees in southern India.</p>")
      goToResults()
    }
  })

  $("#question4Answer11").click(function () {
    if ((question1Answer === "Medium" || question1Answer === "Large") && question2Answer === "More" && question3Answer === "AR") {
      // Origin Congo Organic
      $("#flavourName").text("ORIGIN CONGO ORGANIC")
      $("#mugImg").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/mug-india-c.png") // @Oliver - Done
      $("#mugLink").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/congo-organic-pod-box")
      $("#intensityValue").html("7")
      setTimeout(function () {
        $(".intensity-fill").animate(
          {
            width: "88px"
          },
          850
        )
      }, 2100)
      $("#galleryImg4").attr("src", "https://www.nespresso.com/shared_res/mos/free_html/uk/quiz-page/int-lungo-leggero-gold.png")
      $("#galleryImg4Link").attr("href", "https://www.nespresso.com/pro/uk/en/order/capsules/pro/coffee-lungo-leggero")
      $("#productDetail").html("<p>A smooth organic coffee with roasted cereal notes, nutty aromas and mild fruitiness. Congo Organic is a 50/50 split roast, both splits are dark and short, with the first split being slightly darker and longer to develop more intensity for this coffee.</p>")
      goToResults()
    }
  })

  $("#reloadCTA").click(function () {
    location.reload()
  })
})
